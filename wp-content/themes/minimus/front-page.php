<?php /* Template Name: Main Page */ ?>
<?php get_header(); ?>


  <?php wp_body_open(); ?>
 
    <main class="page__body p-main">
      <div class="container">
        <?php get_template_part('template-parts/content', 'Main Banner'); ?>
        <!-- Main Banner -->
        <div class="main-banner">
          <div class="main-banner__content">
            <?php if (get_field("title")) : ?>
              <div class="main-banner__title">
                <?php the_field('title') ?>
              </div>
            <?php endif; ?>
            <?php if (get_field("description")) : ?>
              <div class="main-banner__description">
                <?php the_field('description') ?>
              </div>
            <?php endif; ?>
          </div>
          <div class="main-banner__img">
            <?php if (get_field("image")) : ?>
              <div class="main-banner__image">
                <img src="<?php the_field('image') ?>" alt="">
              </div>
            <?php endif; ?>
          </div>
        </div>
        <!-- Main Banner -->
        <!-- About Project -->
        <div class="section">
          <?php get_template_part('template-parts/content', 'About Project'); ?>
          <div class="about-project">
            <div class="about-project__img">
              <?php if (get_field("about-project-image")) : ?>
                <img src="<?php the_field('about-project-image') ?>" alt="">
              <?php endif; ?>
            </div>
            <div class="about-project__content">
              <?php if (get_field("about-project-title")) : ?>
                <h2 class="about-project__title">
                  <?php the_field('about-project-title') ?>
                </h2>
              <?php endif; ?>
              <?php if (get_field("about-project-text")) : ?>
                <div class="about-project__text">
                  <?php the_field('about-project-text') ?>
                </div>
              <?php endif; ?>
            </div>
          </div>
        </div>
        <!-- About Project -->
        <!-- form -->
        <?php echo do_shortcode( '[contact-form-7 id="8a9a4a3" title="Contact form 1"]' ); ?>
        <?php the_content() ?>

      </div>

    </main>
 
  <?php get_footer(); ?>