if ($) {
  $(document).ready(function () {
    console.log('ready minimus')
    if ($(this).scrollTop() > 1) {
      $(".page__header").addClass("fixed");
    } else {
      $(".page__header").removeClass("fixed");
    }
    $(window).scroll(function () {
      if ($(this).scrollTop() > 1) {
        $(".page__header").addClass("fixed");
      } else {
        $(".page__header").removeClass("fixed");
      }
    });

    if ($(".js-select").length) {
      $(".js-select").select2({
        minimumResultsForSearch: Infinity,
        // closeOnSelect: false,
      });
    }

    if ($("[data-inputmask]").length) {
      $(":input").inputmask();
    }
    $("input").on("change", function () {
      if ($(this).val().length) {
        $(this).addClass("not-empty");
      } else {
        $(this).removeClass("not-empty");
      }
    });

    $(window).resize(function () {
      $(".js-select").select2({
        minimumResultsForSearch: Infinity,
      });
    });
  
    $('.js-servise-menu-opener').on("click", function () {
      $('.servise-menu__icon').toggleClass('open')
      $('.js-servise-menu').slideToggle()
    })
  
  });
}