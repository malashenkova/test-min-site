<!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 oldie" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!-->
<html <?php language_attributes(); ?>> <!--<![endif]-->

<head>

  <!--=== META TAGS ===-->
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta charset="<?php bloginfo('charset'); ?>" />
  <meta name="description" content="Keywords">
  <meta name="author" content="Name">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

  <!--=== LINK TAGS ===-->
  <link rel="shortcut icon" href="<?php echo THEME_DIR; ?>/img/icons/favicon.ico" />
  <link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS2 Feed" href="<?php bloginfo('rss2_url'); ?>" />
  <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

  <!--=== TITLE ===-->
  <title><?php wp_title(); ?> - <?php bloginfo('name'); ?></title>

  <link rel="shortcut icon" href="<?= THEME_IMAGES; ?>/icons/favicon.svg" type="image/x-icon" />
  <link rel="stylesheet" href="<?= THEME_LIBS; ?>/select2/select2.min.css" />
  <link rel="stylesheet" href="<?= THEME_LIBS; ?>/swiper/swiper.min.css" />
  <link rel="stylesheet" href="<?= THEME_LIBS; ?>/fancybox/jquery.fancybox.min.css" />
  <link rel="stylesheet" href="<?= THEME_LIBS; ?>/jquery-ui.min.css" />
  <link rel="stylesheet" href="<?= THEME_CSS; ?>/style.css" />

  <!--=== WP_HEAD() ===-->
  <?php wp_head(); ?>



</head>

<body <?php body_class(); ?>>

  <!-- HERE GOES YOUR HEADER MARKUP, LIKE LOGO, MENU, SOCIAL ICONS AND MORE -->

  <header class="page__header">
    <div class="header">
      <div class="container">
        <div class="header__content">

          <div class="header__logo">
            <?php
            if (function_exists('the_custom_logo')) {
              $custom_logo_id = get_theme_mod('custom_logo');
              $logo = wp_get_attachment_image_src($custom_logo_id, 'full');
              if (has_custom_logo()) {
                echo '<img src="' . esc_url($logo[0]) . '" alt="' . get_bloginfo('name') . '">';
              } else {
                echo '<div>' . get_bloginfo('name') . '</div>';
              }
            }
            ?>
          </div>

          <?php
          wp_nav_menu(
            array(
              'theme_location' => 'main_menu',
              'container' => 'div',
              'container_class' => 'header__menu',
              'menu_class' => 'main-menu'
            )
          );
          ?>

          <div class="header__socials">
            <div class="socials">

              <?php
              if (get_option('instagram')) {
                $instagram_link = get_option('instagram');
              ?>
                <a href="<?php echo $instagram_link; ?>" target="_blank">
                  <img src="<?= THEME_IMAGES; ?>/icons/instagram.svg" alt="">
                </a>
              <?php
              }
              ?>
              <?php
              if (get_option('twitter/x')) {
                $twitter_link = get_option('twitter/x');
              ?>
                <a href="<?php echo $twitter_link; ?>" target="_blank">
                  <img src="<?= THEME_IMAGES; ?>/icons/twitter-x.svg" alt="">
                </a>
              <?php
              }
              ?>
              <?php
              if (get_option('facebook')) {
                $facebook_link = get_option('facebook');
              ?>
                <a href="<?php echo $facebook_link; ?>" target="_blank">
                  <img src="<?= THEME_IMAGES; ?>/icons/facebook.svg" alt="">
                </a>
              <?php
              }
              ?>
              <?php
              if (get_option('mastodon')) {
                $mastodon_link = get_option('mastodon');
              ?>
                <a href="<?php echo $mastodon_link; ?>" target="_blank">
                  <img src="<?= THEME_IMAGES; ?>/icons/mastodon.svg" alt="">
                </a>
              <?php
              }
              ?>
              <?php
              if (get_option('bluesky')) {
                $bluesky_link = get_option('bluesky');
              ?>
                <a href="<?php echo $bluesky_link; ?>" target="_blank">
                  <img src="<?= THEME_IMAGES; ?>/icons/bluesky.svg" alt="">
                </a>
              <?php
              }
              ?>
            </div>
          </div>

        </div>
      </div>
    </div>
  </header>