    <script src="<?=THEME_LIBS;?>/jquery-3.6.0.min.js"></script>
    <script src="<?=THEME_LIBS;?>/inputmask/jquery.inputmask.min.js"></script>
    <script src="<?=THEME_LIBS;?>/jquery-ui.min.js"></script>
    <script src="<?=THEME_LIBS;?>/jquery.ui.touch-punch.min.js"></script>
    <script src="<?=THEME_LIBS;?>/swiper/swiper.min.js"></script>
    <script src="<?=THEME_LIBS;?>/select2/select2.min.js"></script>
    <script src="<?=THEME_LIBS;?>/fancybox/jquery.fancybox.min.js"></script>
    <script src="<?=THEME_LIBS ;?>/validate/jquery.validate.min.js"></script>

    <script src="<?=THEME_JS ;?>/script.js"></script>
  </body>
</html>
