<?php /* Template Name: Страница "О клинике" */ ?>
<?php get_header(); ?>

<body class="body">
  <?php wp_body_open(); ?>
  <div class="page">
    <?php get_template_part('template-parts/content', 'heading'); ?>

    <main class="page__body">
      <div class="container">
        <!-- Хлебные крошки -->
        <ul class="b-breadcrumbs">
          <li>
            <a href="/">Главная</a>
          </li>
          <li>О клинике</li>
        </ul>
        <!-- /Хлебные крошки -->
        <h1><?php the_title() ?></h1>
        <!-- О клинике -->
        <div class="b-clinic">
          <div class="clinic__left">
            <?php the_field('about-text') ?>
          </div>
          <?php if (have_rows('slider')) : ?>
            <div class="clinic__right">
              <!-- О клинике слайдер -->
              <div class="swiper b-about-slider js-about-slider">
                <div class="swiper-wrapper">

                  <?php while (have_rows('slider')) : the_row();
                    $image = get_sub_field('image');
                  ?>

                    <div class="swiper-slide about-slider__slide">
                      <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
                    </div>

                  <?php endwhile; ?>

                </div>
                <div class="about-slider__nav">
                  <div class="about-slider__prev js-about-slider-prev">
                    <svg width="80" height="80" viewBox="0 0 80 80" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <circle cx="40" cy="40" r="39.5" stroke="#DDE9EC" />
                      <path d="M37.768 32L38.6994 33.0536L30.5072 40.2953H52V41.7015H30.5072L38.6994 48.9432L37.768 49.9968L28 41.3622V40.6346L37.768 32Z" fill="white" />
                    </svg>
                  </div>
                  <div class="about-slider__next js-about-slider-next">
                    <svg width="80" height="80" viewBox="0 0 80 80" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <circle r="39.5" transform="matrix(-1 0 0 1 40 40)" stroke="#DEEAE7" />
                      <path d="M42.232 32L41.3006 33.0536L49.4928 40.2953H28V41.7015H49.4928L41.3006 48.9432L42.232 49.9968L52 41.3622V40.6346L42.232 32Z" fill="white" />
                    </svg>
                  </div>
                </div>
                <div class="swiper-pagination about-slider__pagination js-about-slider-pagination"></div>
              </div>
              <!-- /О клинике слайдер -->
            </div>
          <?php endif; ?>
        </div>
        <!-- /О клинике -->
      </div>
      <div class="blue-section">
        <div class="container">
          <!-- Преимущества -->
          <div class="b-competitiveness">
            <div class="competitiveness__top">
              <h2><?php the_field('competitiveness-title') ?></h2>
            </div>
            <?php if (have_rows('competitiveness')) : ?>
              <div class="competitiveness__items">
                <?php while (have_rows('competitiveness')) : the_row();
                  $image = get_sub_field('image');
                ?>
                  <div class="competitiveness__item">
                    <div class="competitiveness__title">
                      <?php the_sub_field('text') ?>
                      <span class="competitiveness__circle revealator-zoomin"></span>
                    </div>
                    <div class="competitiveness__img">
                      <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
                    </div>
                  </div>
                <?php endwhile; ?>
              </div>
            <?php endif; ?>
          </div>
          <!-- /Преимущества -->
        </div>
      </div>
      <div class="container">
        <h2 class="small-mb">
          <?php the_field('directions-title') ?>
        </h2>
        <!--  Особое внимание  -->
        <?php if (have_rows('directions-list')) : ?>
          <div class="b-atention">
            <?php while (have_rows('directions-list')) : the_row(); ?>
              <div class="atention__item"><?php the_sub_field('text') ?></div>
            <?php endwhile; ?>
          </div>
        <?php endif; ?>
        <!--  Особое внимание  -->
        <!-- Информация по центру -->
        <?php if (get_field("directions-marker")) : ?>
          <div class="b-bird-info">
            <?php the_field('directions-marker') ?>
          </div>
        <?php else : ?>
        <?php endif; ?>
        <!-- /Информация по центру -->
        <?php if (have_rows('treatment-list', 2)) :  $counter = 0; ?>

          <!-- У нас есть все для эффективного лечения -->
          <div class="b-treatment">
            <!-- Первый цикл слайдов -->
            <ul class="treatment__tabs">
              <?php while (have_rows('treatment-list', 2)) : the_row();
              ?>
                <li class="treatment__tab js-treatment-tab">
                  <a class="js-treatment-link <?php if ($counter == 0) { ?>active<?php } ?>" href="#treatment-<?php echo get_row_index() ?>"><?php the_sub_field('title') ?></a>

                  <div class="treatment__inside js-treatment-inside <?php if ($counter == 0) { ?>active<?php } ?>" <?php if ($counter != 0) { ?>style="display: none" <?php } ?>>
                    <?php if (have_rows('slider')) : ?>
                      <div class="swiper treatment__slider js-treatment">
                        <div class="swiper-wrapper treatment__wrapper">
                          <?php while (have_rows('slider')) : the_row();
                            $image = get_sub_field('image'); ?>
                            <div class="swiper-slide treatment__slide">
                              <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
                            </div>
                          <?php endwhile; ?>

                        </div>
                        <div class="treatment__nav">
                          <div class="treatment__prev js-treatment-prev">
                            <svg width="60" height="60" viewBox="0 0 60 60" fill="none" xmlns="http://www.w3.org/2000/svg">
                              <circle cx="30" cy="30" r="29.5" stroke="#DDE9EC" />
                              <path d="M28.326 24L29.0245 24.7902L22.8804 30.2215H39V31.2761H22.8804L29.0245 36.7074L28.326 37.4976L21 31.0217V30.476L28.326 24Z" fill="white" />
                            </svg>
                          </div>
                          <div class="treatment__next js-treatment-next">
                            <svg width="60" height="60" viewBox="0 0 60 60" fill="none" xmlns="http://www.w3.org/2000/svg">
                              <circle r="29.5" transform="matrix(-1 0 0 1 30 30)" stroke="#DDE9EC" />
                              <path d="M31.674 24L30.9755 24.7902L37.1196 30.2215H21V31.2761H37.1196L30.9755 36.7074L31.674 37.4976L39 31.0217V30.476L31.674 24Z" fill="white" />
                            </svg>
                          </div>
                        </div>
                      </div>
                      <?php the_sub_field('text') ?>
                  </div>
                <?php endif; ?>
                </li>
              <?php $counter++;
              endwhile; ?>
            <?php endif; ?>
            </ul>
            <!-- второй цикл слайдов -->
            <?php if (have_rows('treatment-list', 2)) : $counter = 0; ?>
              <?php while (have_rows('treatment-list', 2)) : the_row(); ?>
                <div class="treatment__content js-treatment-content <?php if ($counter == 0) { ?>active<?php } ?>" id="treatment-<?php echo get_row_index() ?>" <?php if ($counter != 0) { ?>style="display: none" <?php } ?>" ">
              <?php if (have_rows('slider')) : ?>
              <div class=" swiper treatment__slider js-treatment">
                  <div class="swiper-wrapper treatment__wrapper">
                    <?php while (have_rows('slider')) : the_row();
                      $image = get_sub_field('image'); ?>
                      <div class="swiper-slide treatment__slide">
                        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
                      </div>
                    <?php endwhile; ?>

                  </div>
                  <div class="treatment__nav">
                    <div class="treatment__prev js-treatment-prev">
                      <svg width="60" height="60" viewBox="0 0 60 60" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="30" cy="30" r="29.5" stroke="#DDE9EC" />
                        <path d="M28.326 24L29.0245 24.7902L22.8804 30.2215H39V31.2761H22.8804L29.0245 36.7074L28.326 37.4976L21 31.0217V30.476L28.326 24Z" fill="white" />
                      </svg>
                    </div>
                    <div class="treatment__next js-treatment-next">
                      <svg width="60" height="60" viewBox="0 0 60 60" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle r="29.5" transform="matrix(-1 0 0 1 30 30)" stroke="#DDE9EC" />
                        <path d="M31.674 24L30.9755 24.7902L37.1196 30.2215H21V31.2761H37.1196L30.9755 36.7074L31.674 37.4976L39 31.0217V30.476L31.674 24Z" fill="white" />
                      </svg>
                    </div>
                  </div>
                </div>
              <?php endif; ?>
          </div>
        <?php $counter++;
              endwhile; ?>
      <?php endif; ?>
      </div>
      <!-- /У нас есть все для эффективного лечения -->
  </div>
  <div class="blue-section">
    <div class="container">
      <!-- Отзывы -->
      <div class="b-reviews">
        <div class="reviews__info">
          <?php the_field('reviews-title') ?>
        </div>
        <?php if (have_rows('reviews')) : ?>
          <div class="reviews__items js-reviews">
            <?php while (have_rows('reviews')) : the_row();
              $image = get_sub_field('image'); ?>
              <div class="reviews__item js-review">
                <div class="reviews__top">
                  <div class="reviews__img">
                    <?php $image = get_sub_field('image');
                    $size = 'review';
                    if ($image) {
                      echo wp_get_attachment_image($image, $size);
                    }
                    ?>
                  </div>
                  <div class="reviews__name"><?php the_sub_field('name') ?></div>
                </div>
                <div class="reviews__text">
                  <?php the_sub_field('text') ?>
                </div>
                <div class="reviews__date"><?php the_sub_field('data') ?> в <?php the_sub_field('time') ?></div>
              </div>
            <?php endwhile; ?>
          </div>
          <!-- <div class="reviews__btn b-btn">Показать еще</div> -->
        <?php endif; ?>
      </div>
      <!-- Отзывы -->
    </div>
  </div>

  <div class="container">
    <!-- Лицензии и сертификаты -->
    <h2>Сертификаты и лицензии</h2>
    <div class="swiper b-sertificates js-sertificates">
      <div class="swiper-wrapper">
        <?php
        $images = get_field('liczenzii', 433);
        if ($images) : ?>
          <?php foreach ($images as $image) : ?>
            <div class="swiper-slide sertificates__slide">
              <a href="<?php echo $image['url']; ?>" data-fancybox="sertificates">
                <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
              </a>
            </div>
          <?php endforeach; ?>
        <?php endif; ?>
        <?php
        $images = get_field('sert', 433);
        if ($images) : ?>
          <?php foreach ($images as $image) : ?>
            <div class="swiper-slide sertificates__slide">
              <a href="<?php echo $image['url']; ?>" data-fancybox="sertificates">
                <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
              </a>
            </div>
          <?php endforeach; ?>
        <?php endif; ?>
      </div>
      <div class="sertificates__nav">
        <div class="sertificates__prev js-sertificates-prev">
          <svg width="70" height="70" viewBox="0 0 70 70" fill="none" xmlns="http://www.w3.org/2000/svg">
            <g opacity="0.8">
              <circle opacity="0.5" cx="35" cy="35" r="34" stroke="url(#paint0_linear_52_1845)" />
              <path d="M26.9313 35.9465H44.7002V35.7512H26.9313H25.6108L26.6002 34.8766L33.1889 29.0523L33.0595 28.906L25.3002 35.765V35.9327L33.0595 42.7917L33.1889 42.6454L26.6002 36.8211L25.6108 35.9465H26.9313Z" stroke="url(#paint1_linear_52_1845)" />
            </g>
            <defs>
              <linearGradient id="paint0_linear_52_1845" x1="1" y1="34.9993" x2="69" y2="34.9993" gradientUnits="userSpaceOnUse">
                <stop stop-color="#136192" />
                <stop offset="1" stop-color="#008255" />
              </linearGradient>
              <linearGradient id="paint1_linear_52_1845" x1="45.2002" y1="35.8487" x2="24.8002" y2="35.8487" gradientUnits="userSpaceOnUse">
                <stop stop-color="#136192" />
                <stop offset="1" stop-color="#008255" />
              </linearGradient>
            </defs>
          </svg>
        </div>
        <div class="sertificates__next js-sertificates-next">
          <svg width="68" height="68" viewBox="0 0 68 68" fill="none" xmlns="http://www.w3.org/2000/svg">
            <g opacity="0.8">
              <circle opacity="0.5" r="33.5" transform="matrix(-1 0 0 1 34 34)" stroke="url(#paint0_linear_52_1841)" />
              <path d="M42.0687 34.9465H24.2998V34.7512H42.0687H43.3892L42.3998 33.8766L35.8111 28.0523L35.9405 27.906L43.6998 34.765V34.9327L35.9405 41.7917L35.8111 41.6454L42.3998 35.8211L43.3892 34.9465H42.0687Z" stroke="url(#paint1_linear_52_1841)" />
            </g>
            <defs>
              <linearGradient id="paint0_linear_52_1841" x1="3.10714e-07" y1="33.9993" x2="68" y2="33.9993" gradientUnits="userSpaceOnUse">
                <stop stop-color="#136192" />
                <stop offset="1" stop-color="#008255" />
              </linearGradient>
              <linearGradient id="paint1_linear_52_1841" x1="23.7998" y1="34.8487" x2="44.1998" y2="34.8487" gradientUnits="userSpaceOnUse">
                <stop stop-color="#136192" />
                <stop offset="1" stop-color="#008255" />
              </linearGradient>
            </defs>
          </svg>
        </div>
      </div>
      <div class="swiper-pagination sertificates__pagination js-sertificates-pagination"></div>
    </div>
    <!-- Лицензии и сертификаты -->
  </div>
  <?php
  $post_object = get_field('doc');
  if ($post_object) :
    $post = $post_object;
    setup_postdata($post);
  ?>
    <div class="blue-section">
      <div class="container">
        <h2>Генеральный директор</h2>
        <!-- Карточка специалиста -->
        <div class="b-specialist">
          <div class="specialist__img">
            <?php
            $image = get_field('image');
            if (!empty($image)) : ?>
              <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
            <?php endif; ?>
          </div>
          <div class="specialist__content">
            <div class="specialist__information">
              <div class="specialist__name">
                <?php the_title() ?>
              </div>
              <div class="specialist__position">
                Генеральный директор клиники «Джамси»,оперирующий
                травматолог–ортопед
              </div>
              <?php if (get_field("experience")) : ?>
                <div class="specialist__experience">Стаж работы <?php the_field('experience'); ?></div>
              <?php else : ?>
              <?php endif; ?>
            </div>
            <div class="specialist__info">
              <?php the_field('about') ?>
            </div>
          </div>
        </div>
        <!-- /Карточка специалиста -->
      </div>
    </div>
    <?php wp_reset_postdata(); ?>
  <?php endif; ?>


  <?php
  $args = array(
    'post_type'      => 'doctor',
    'post_status'    => 'publish',
    'posts_per_page' => 8,
    'order' => 'ASC'
  );

  $query = new WP_Query($args);

  if ($query->have_posts()) { ?>
    <div class="container">
      <h2>Наши специалисты</h2>
      <!-- Врачи -->
      <div class="b-doctors">
        <div class="doctors__items">
          <?php
          while ($query->have_posts()) {
            $query->the_post();
          ?>
            <div class="doctors__item">
              <div class="doctors__img">
                <?php
                $image = get_field('image');
                if (!empty($image)) : ?>
                  <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                <?php endif; ?>
              </div>
              <div class="doctors__name"><?php the_title() ?></div>
              <div class="doctors__content">
                      <?php if (get_field("dir-position")) : ?>
                        <p>
                          <?php the_field('dir-position'); ?>
                        </p>
                      <?php else : ?>
                      <?php endif; ?>

                      <?php if (get_field("positions")) : ?>
                        <p>
                          <?php the_field('positions'); ?>
                        </p>
                      <?php else : ?>
                      <?php endif; ?>

                      <?php if (get_field("experience")) : ?>
                        <p>Стаж работы <?php the_field('experience'); ?></p>
                      <?php else : ?>
                      <?php endif; ?>
              </div>
              <a href="<?php the_permalink() ?>" class="doctors__link">&nbsp;</a>
            </div>
          <?php
          }
          ?>
        </div>
        <a href="/doctors/" class="b-btn doctors__btn">Показать еще</a>
      </div>
      <!-- /Врачи -->
    </div>
  <?php
  } else {
    echo '';
  }
  wp_reset_postdata();
  ?>
  </main>






  <?php
  get_footer();
